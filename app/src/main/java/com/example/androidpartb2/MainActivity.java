package com.example.androidpartb2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText login, email, phoneNumber, password, confirmPassword;

    Button buttonValidation;
    TextView operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login =findViewById(R.id.editLogin);
        email = findViewById(R.id.emailEdit);
        phoneNumber = findViewById(R.id.editPhoneNumber);
        password = findViewById(R.id.editPassword);
        confirmPassword = findViewById(R.id.confirmPassword);

        buttonValidation = findViewById(R.id.validation);
        buttonValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().equals("")) {
                    operation.setText("Edit login");
                }
                    else if (email.getText().toString().equals(""))   {
                        operation.setText("Edit email");
                }
                    else if (phoneNumber.getText().toString().equals("")) {
                        operation.setText("Edit phone number");
                    }
                    else if (password.getText().toString().equals(""))    {
                        operation.setText("Edit PASS");
                }
                    else if (confirmPassword.getText().toString().equals("")) {
                        operation.setText("Confirm your PASS");
                }
                    else if (!password.getText().toString().equals(confirmPassword.toString())){
                        operation.setText("Try to confirm your PASS");
                }
                    else operation.setText("Congrats");

            }
        });
        operation = findViewById(R.id.operation);

    }


}
